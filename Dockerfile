FROM archlinux

RUN patched_glibc=glibc-linux4-2.33-4-x86_64.pkg.tar.zst && \ 
	curl -LO "https://repo.archlinuxcn.org/x86_64/$patched_glibc" && \
	bsdtar -C / -xvf "$patched_glibc"
RUN pacman -Syu base-devel sudo git vim bash-completion --noconfirm && \
	yes | pacman -Scc
RUN useradd user && usermod -p "" user && mkdir /home/user && \ 
	chown user:user /home/user && \ 
	echo "user ALL=(ALL) ALL" > /etc/sudoers.d/user 
USER user
WORKDIR /home/user
